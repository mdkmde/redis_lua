--[[
https://gitlab.com/mdkmde/redis_lua/ - matthias (at) koerpermagie.de

Copyright (c) ISC License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
]]

-- Module config
local DEFAULT_REDIS_SERVER_NAME = "localhost"
local DEFAULT_REDIS_SERVER_PORT = 6379
local CRLF = "\r\n"
-- Redis error messages
local REDIS_AUTH_NOT_REQUIRED =  "ERR Client sent AUTH, but no password is set"
local REDIS_AUTH_REQUIRED = "NOAUTH Authentication required."

local pairs, pcall, setmetatable, tonumber, type = pairs, pcall, setmetatable, tonumber, type
local os = {time = os.time}
local string = {find = string.find, format = string.format, gsub = string.gsub, sub = string.sub, upper = string.upper}
local table = {concat = table.concat}
local socket = require("cqueues.socket")
socket.setmode("b", "b")
_ENV = nil

local t_reconnect = {closed = true, timeout = true}
local getResult
getResult = function(it_redis_metadata, in_result, it_named_response)
-- returns result data table {{}[, {}]} or ({{nil, error}})
-- requires redis data object, number of results, optional named response table
  local log = it_redis_metadata.t_log
  local t_results = {}
  for n_i = 1, in_result do
    local s_result_name = it_named_response and (type(it_named_response[n_i]) == "string") and it_named_response[n_i] or n_i
    local s_result, s_err = it_redis_metadata.scon:read("*L")
    if not(s_result) then
      if t_reconnect[s_err] then
        it_redis_metadata.scon = nil
      end
      return {{nil, s_err}}
    end
    s_result = string.sub(s_result, 1, -1 - #CRLF)
    it_redis_metadata.t_stat.r = it_redis_metadata.t_stat.r + #s_result
    log.SOCKET("redis.getResult(%d) - redis.server:read() -> \"%s%s\"", it_redis_metadata.n_id, s_result, CRLF)
    local s_reply_code = string.sub(s_result, 1, 1)
    s_result = string.sub(s_result, 2)
    if s_reply_code == "+" then
      -- single line
      log.DEBUG("redis.getResult(%d) - redis server: single line: {\"%s\"}", it_redis_metadata.n_id, s_result)
      t_results[s_result_name] = {s_result}
    elseif s_reply_code == "-" then
      -- error message
      if it_redis_metadata.t_auth and s_result == REDIS_AUTH_NOT_REQUIRED then
        it_redis_metadata.t_auth = nil
        log.NOTICE("redis.getResult(%d) - authentication not required", it_redis_metadata.n_id)
        t_results[s_result_name] = {"OK"}
      else
        log.ERROR("redis.getResult(%d) - redis server: error message: \"%s\"", it_redis_metadata.n_id, s_result)
        return {{nil, s_result}}
      end
    elseif s_reply_code == ":" then
      -- integer number
      log.DEBUG("redis.getResult(%d) - redis server: integer number: {%s}", it_redis_metadata.n_id, s_result)
      t_results[s_result_name] = {tonumber(s_result)}
    elseif s_reply_code == "$" then
      -- bulk
      local n_len = tonumber(s_result)
      if 0 <= n_len then
        local s_bulk_reply, s_err = it_redis_metadata.scon:read(n_len + #CRLF)
        if not(s_bulk_reply) then
          return {{nil, s_err}}
        end
        it_redis_metadata.t_stat.r = it_redis_metadata.t_stat.r + #s_bulk_reply
        t_results[s_result_name] = {string.sub(s_bulk_reply, 1, n_len)}
        log.SOCKET("redis.getResult(%d) - redis.server:read() -> \"%s\"", it_redis_metadata.n_id, s_bulk_reply)
        log.DEBUG("redis.getResult(%d) - redis server: bulk: %d{\"%s\"}", it_redis_metadata.n_id, n_len, t_results[s_result_name][1])
      else
        t_results[s_result_name] = {false} --should be "nil", but table length won't work
        log.DEBUG("redis.getResult(%d) - redis server: bulk: %d{}", it_redis_metadata.n_id, n_len)
      end
    elseif s_reply_code == "*" then
      -- multi-bulk / array
      local n_result = tonumber(s_result)
      log.DEBUG("redis.getResult(%d) - redis server: multi-bulk: %s{", it_redis_metadata.n_id, s_result)
      local t_subresults = getResult(it_redis_metadata, n_result)
      s_result_name = n_i
      local b_result
      local s_sub_result_name = {}
      if it_named_response and it_named_response[n_i] then
        if type(it_named_response[n_i]) == "table" then
          s_sub_result_name = it_named_response[n_i]
          if type(it_named_response[n_i][0]) == "string" then
            s_result_name = it_named_response[n_i][0]
          end
        elseif type(it_named_response[n_i]) == "string" then
          s_result_name = it_named_response[n_i]
        elseif type(it_named_response[n_i]) == "boolean" then
          b_result = true
        end
      end
      t_results[s_result_name] = {}
      for n_j = 1, n_result do
        if b_result then
          t_results[s_result_name][t_subresults[n_j][1]] = true
        else
          t_results[s_result_name][(type(s_sub_result_name[n_j]) == "string") and s_sub_result_name[n_j] or n_j] = t_subresults[n_j][1]
        end
      end
      log.DEBUG("redis.getResult(%d) - } -- multi-bulk", it_redis_metadata.n_id)
    end
  end
  return t_results
end

local prepareQuery = function(it_redis_metadata, it_query)
-- returns repared query table {query count, query string} or (nil, error)
-- requires current redis metadat table, query table
  local log = it_redis_metadata.t_log
  local t_cmds = {}
  for _, t_cmdline in pairs(it_query) do
    if not(type(t_cmdline) == "table") then
      log.NOTICE("redis.prepareQuery(%d) - invalid type: %s (table) arg[%d]", it_redis_metadata.n_id, type(t_cmdline), #t_cmds)
      return nil, string.format("invalid type: %s (table) arg[%d]", type(t_cmdline), #t_cmds)
    end
    local t_cmd = {string.format("*%d%s", #t_cmdline, CRLF)}
    t_cmdline[1] = string.upper(t_cmdline[1] or "")
    for _, s_cmdarg in pairs(t_cmdline) do
      if not(type(s_cmdarg) == "string") then
        log.NOTICE("redis.prepareQuery(%d) - invalid type: %s (string) arg[%d][%d]", it_redis_metadata.n_id, type(s_cmdarg), #t_cmds + 1, #t_cmd)
        return nil, string.format("invalid type: %s (string) arg[%d][%d]", type(s_cmdarg), #t_cmds + 1, #t_cmd)
      end
      t_cmd[(#t_cmd + 1)] = string.format("$%d%s%s%s", #s_cmdarg, CRLF, s_cmdarg, CRLF)
    end
    t_cmds[(#t_cmds + 1)] = table.concat(t_cmd)
    if log.DEBUG then
      log.DEBUG("redis.prepareQuery(%d) - got query: \"%s\"", it_redis_metadata.n_id, string.sub(string.gsub(t_cmds[#t_cmds], string.format("[%s]*[*$][0-9]*%s", CRLF, CRLF), " "), 3))
    end
    local t_special_cmd = {
      AUTH = {"t_auth", "authentication for redis"},
      SELECT = {"t_db", "select database"},
    }
    if t_special_cmd[t_cmdline[1]] then
      it_redis_metadata[t_special_cmd[t_cmdline[1]][1]] = {n_query = 1, s_query = t_cmds[#t_cmds], s_value = t_cmdline[2]}
      log.INFO("redis.prepareQuery(%d) - %s: %s", it_redis_metadata.n_id, t_special_cmd[t_cmdline[1]][2], t_cmdline[2])
    end
  end
  return {n_query = #t_cmds, s_query = table.concat(t_cmds)}
end

local doQuery = function(it_redis_metadata, it_prepared_query, it_named_response)
-- returns result data table {{}[, {}]} or (nil, error)
-- requires current redis metadat table, query string, optional named response table
  local _, s_err = it_redis_metadata.scon:write(it_prepared_query.s_query)
  if s_err then
    if t_reconnect[s_err] then
      it_redis_metadata.scon = nil
    end
    return {{nil, s_err}}
  end
  it_redis_metadata.t_log.SOCKET("redis.doQuery(%d) - redis.server:write(\"%s\")", it_redis_metadata.n_id, it_prepared_query.s_query)
  it_redis_metadata.t_stat.s = it_redis_metadata.t_stat.s + it_prepared_query.n_query
  return getResult(it_redis_metadata, it_prepared_query.n_query, it_named_response)
end

local doConnect = function(it_redis_metadata, ib_reconnect)
-- returns nil or nil, error string
  local log = it_redis_metadata.t_log
  local _, s_err
  it_redis_metadata.scon, s_err = socket.connect(it_redis_metadata.t_socket_config)
  if not(it_redis_metadata.scon) then
    local s_sock = it_redis_metadata.t_socket_config.path and it_redis_metadata.t_socket_config.path
      or string.format("%s:%d", it_redis_metadata.t_socket_config.host, it_redis_metadata.t_socket_config.port)
    log.ERROR("redis.doConnect(%d) - can not connect socket \"%s\": %s", it_redis_metadata.n_id, s_err)
    return nil, string.format("Can not connect socket \"%s\": %s", s_err)
  end
  if it_redis_metadata.t_auth then
    if not(pcall(doQuery, it_redis_metadata, it_redis_metadata.t_auth)) then
      log.ERROR("redis.doConnect(%d) - can not authenticate with phrase: %s", it_redis_metadata.n_id, it_redis_metadata.t_auth.s_value)
      return nil, string.format("Can not authenticate with phrase: %s", it_redis_metadata.t_auth.s_value)
    end
  end
  if it_redis_metadata.t_db then
    _, s_err = pcall(doQuery, it_redis_metadata, it_redis_metadata.t_db)
    if type(s_err) == "string" then
      log.ERROR("redis.doConnect(%d) - can not select database \"%s\": %q", it_redis_metadata.n_id, it_redis_metadata.t_db.s_value, s_err)
      return nil, string.format("Can not select database \"%s\": %q", it_redis_metadata.t_db.s_value, s_err)
    end
  elseif not(it_redis_metadata.t_auth) then
    -- check "PING" only, if connection was not tested by AUTH or SELECT command
    _, s_err = pcall(doQuery, it_redis_metadata, it_redis_metadata.t_query_ping)
    if type(s_err) == "string" then
      if s_err == REDIS_AUTH_REQUIRED then
        log.WARN("redis.doConnect(%d) - not authenticated", it_redis_metadata.n_id)
      else
        log.ERROR("redis.doConnect(%d) - unknown database error, ping failed: %s", it_redis_metadata.n_id, s_err)
        return nil, string.format("Unknown database error, ping failed: %s", s_err)
      end
    end
  end
  if not(ib_reconnect) then
    log.INFO("redis.doConnect(%d) - new redis connection to \"redis://%s%s%s/%s\" established", it_redis_metadata.n_id, it_redis_metadata.t_socket_config.path or it_redis_metadata.t_socket_config.host, it_redis_metadata.t_socket_config.path and "=" or ":", it_redis_metadata.t_socket_config.path and "" or it_redis_metadata.t_socket_config.port, it_redis_metadata.t_db and it_redis_metadata.t_db.s_value or "0")
  end
end

local query
query = function(it_redis_metadata, it_query, it_named_response)
-- returns result data table {{}[, {}]} or (nil, error)
-- requires current redis data object, query table {{"CMD", "arg1", ..}, } | prepared query table, optional named response table
  local log = it_redis_metadata.t_log
  if not(type(it_query) == "table") or (#it_query == 0) then
    log.NOTICE("redis.query(%d) - no query", it_redis_metadata.n_id)
    return {{nil, "no query"}}
  end
  local b_reconnect = false
  if not(it_redis_metadata.scon) then
    b_reconnect = true
    log.INFO("redis.query(%d) - lost redis connection to \"redis://%s%s%s/%s\" (%dq %dB/%dB %ds) - reconnect", it_redis_metadata.n_id, it_redis_metadata.t_socket_config.path or it_redis_metadata.t_socket_config.host, it_redis_metadata.t_socket_config.path and "=" or ":", it_redis_metadata.t_socket_config.path and "" or it_redis_metadata.t_socket_config.port, it_redis_metadata.t_db and it_redis_metadata.t_db.s_value or "0", it_redis_metadata.t_stat.q, it_redis_metadata.t_stat.s, it_redis_metadata.t_stat.r, os.time() - it_redis_metadata.t_stat.t)
    local _, s_err = doConnect(it_redis_metadata, b_reconnect)
    if s_err then
      log.ERROR("redis.query(%d) - connection failed", it_redis_metadata.n_id)
      it_redis_metadata.scon = nil
      return {{nil, s_err}}
    end
    log.DEBUG("redis.query(%d) - connection established", it_redis_metadata.n_id)
  end
  local t_prepared_query, s_err = it_query.n_query and it_query or prepareQuery(it_redis_metadata, it_query)
  if not(t_prepared_query) then
    return {{nil, s_err}}
  end
  local t_result = doQuery(it_redis_metadata, t_prepared_query, it_named_response)
  if (t_result[1] and (t_result[1][1] == nil) and ((type(t_result[1][2]) == "string") and t_reconnect[t_result[1][2]])) and not(b_reconnect) then
    return query(it_redis_metadata, it_query, it_named_response)
  end
  it_redis_metadata.t_stat.q = it_redis_metadata.t_stat.q + 1
  return t_result
end

local close = function(it_redis_metadata)
-- returns nothing
  if it_redis_metadata.scon then
    it_redis_metadata.scon:write(string.format("QUIT%s", CRLF))
    it_redis_metadata.scon:close()
    it_redis_metadata.scon = nil
    it_redis_metadata.t_log.INFO("redis.close(%d) - close redis connection to \"redis://%s%s%s/%s\" (%dq %dB/%dB %ds)", it_redis_metadata.n_id, it_redis_metadata.t_socket_config.path or it_redis_metadata.t_socket_config.host, it_redis_metadata.t_socket_config.path and "=" or ":", it_redis_metadata.t_socket_config.path and "" or it_redis_metadata.t_socket_config.port, it_redis_metadata.t_db and it_redis_metadata.t_db.s_value or "0", it_redis_metadata.t_stat.q, it_redis_metadata.t_stat.s, it_redis_metadata.t_stat.r, os.time() - it_redis_metadata.t_stat.t)
  else
    it_redis_metadata.t_log.DEBUG("redis.close(%d) - already closed", it_redis_metadata.n_id)
  end
end

local n_redis_connection_id = 1
local connect = function(is_server_dsn, it_log)
-- returns redis table
-- server DSN: redis://[auth@][[server][:port]|[socket=]][/db], optional log
  local nolog = function() end
  local log = it_log or setmetatable({}, {__index = function() return nolog; end})
  local t_redis_metadata = {n_id = n_redis_connection_id, t_log = log, t_stat = {q = 0, r = 0, s = 0, t = os.time()}}
  n_redis_connection_id = n_redis_connection_id < 100000 and n_redis_connection_id + 1 or 1
  t_redis_metadata.t_socket_config = {host = DEFAULT_REDIS_SERVER_NAME, port = DEFAULT_REDIS_SERVER_PORT}
  t_redis_metadata.t_query_ping = prepareQuery(t_redis_metadata, {{"PING"}})
  if type(is_server_dsn) == "string" then
    log.DEBUG("redis.connect(%d) - got server DSN: %q", t_redis_metadata.n_id, is_server_dsn)
    local s_server_dsn = string.gsub(is_server_dsn, "[^-:/@_A-Za-z0-9.=]", "")
    local _, n_match = string.gsub(s_server_dsn, "^redis://([^@:/]*)(@?)([^:/]*):?([^/]*)([^=]*)(=?)/?(.*)$", function(is_auth, is_auth_set, is_server_name, is_server_port, is_socket_name, is_socket_set, is_db)
      if is_auth_set == "@" then
        log.DEBUG("redis.connect(%d) - got server auth: %s", t_redis_metadata.n_id, is_auth)
        prepareQuery(t_redis_metadata, {{"AUTH", is_auth}})
      else
        is_server_name = is_auth
      end
      if is_socket_set == "=" then
        log.DEBUG("redis.connect(%d) - got socket name: %s", t_redis_metadata.n_id, is_socket_name)
        t_redis_metadata.t_socket_config = {path = is_socket_name}
      else
        is_db = string.sub(is_socket_name, 2)
        if not(is_server_name == "") then
          log.DEBUG("redis.connect(%d) - got server name: %s", t_redis_metadata.n_id, is_server_name)
          t_redis_metadata.t_socket_config.host = is_server_name
        end
        if tonumber(is_server_port) then
          log.DEBUG("redis.connect(%d) - got server port: %s", t_redis_metadata.n_id, is_server_port)
          t_redis_metadata.t_socket_config.port = tonumber(is_server_port)
        end
      end
      if not(is_db == "") then
        log.DEBUG("redis.connect(%d) - got database: %s", t_redis_metadata.n_id, is_db)
        prepareQuery(t_redis_metadata, {{"SELECT", is_db}})
      end
    end)
    if not(n_match == 1) then
      return nil, string.format("ERROR: Cannot parse server DSN: %q should be: \"redis://[auth@][[server][:port]|[socket=]][/db]\"", is_server_dsn)
    end
  end
  local _, s_err = doConnect(t_redis_metadata)
  if s_err then
    return nil, s_err
  end
  local query = function(...) return query(t_redis_metadata, ...); end
  local prepareQuery = function(...) return prepareQuery(t_redis_metadata, ...); end
  local t_redis = {close = function() close(t_redis_metadata); end, q = query, query = query, prepareQuery = prepareQuery}
  local t_redis_metadata = {__gc = function() if t_redis_metadata.scon then close(t_redis_metadata); end; end}
  return setmetatable(t_redis, t_redis_metadata)
end

return {
  connect = connect, --[=[ ( optional dsn: redis://[auth@][[server][:port]|[socket=]][/db], optional log (flog.lua)
    returns redis table: {
      q[uery](
          query table: {{"CMD", "arg1", .. }[, {"CMD", ..}, ..]},
          optional response table for named result / true - see Readme.lua for details
        )
        returns result data table {{}[, {}]} or nil, error message
      close()
    } ]=]
}


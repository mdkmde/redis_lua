#!/usr/bin/env lua
local redis = require("redis")
-- set 0 to disable os.execute(string.format("sleep %d", RECONNECT_TIMEOUT))
local RECONNECT_TIMEOUT = 12
local REDIS_CONNECTION="localhost:6379"
--local REDIS_CONNECTION="/tmp/redis.sock="
local TEST_AUTH = "foobared"
local TEST_DB = "15"
local result = nil
local log = nil
---[==[
log = require("flog").init({
  ERROR = "stdout",
  WARN = "stdout",
  NOTICE = "stdout",
  INFO = "stdout",
--  SOCKET = "stdout",
--  DEBUG = "stdout",
})
--]==]
-- redis.connect("redis://[auth@][[server][:port]|[socket=]][/db]"[, log])
local r = redis.connect(string.format("redis://%s", REDIS_CONNECTION), log)
-- will give an error log message, if auth fails
r.q({{"DBSIZE"}})
-- authentication (no problem, if not required)
if not(r.q({{"AUTH", TEST_AUTH}})[1][1] == "OK") then
  print(string.format("Authentication failed.. test aborted", TEST_AUTH))
  os.exit(1)
end
print(string.format("DBSIZE -> %d", r.q({{"DBSIZE"}})[1][1]))

-- single line reply
if not(r.q({{"SELECT", TEST_DB}})[1][1] == "OK") then
  print(string.format("Selection of DB %s failed.. test aborted", TEST_DB))
  os.exit(1)
end
 -- integer number reply
if not(r.q({{"DBSIZE"}})[1][1] == 0) then
  print(string.format("DB %s contains data.. test aborted", TEST_DB))
  os.exit(1)
end
print(string.format("SET key_1 = value_1 -> %s", r.q({{"SET", "key_1", "value_1"}})[1][1]))
-- set multi key, command pipelining -- returns {{"OK"}{"OK"}}
result = r.q({{"SET", "key_2", "value_2"}, {"SET", "key_3", "value_3"}})
print(string.format("number of results: %i{", #result))
for n, v in pairs(result) do print(string.format(" SET key_%d = value_%d -> %s", (n + 1), (n + 1), v[1])); end
print("}");
-- bulk reply
print(string.format("GET key_1 -> \"%s\"", r.q({{"GET", "key_1"}})[1][1]))
-- multi-bulk reply
result = r.q({{"MGET", "key_2", "key_3"}})
print(string.format("MGET key_2 key_3 = %i{%i{\"%s\" \"%s\"}}", #result, #result[1], result[1][1], result[1][2]))
-- open second connection
local r1 = redis.connect(string.format("redis://%s@%s/%s", TEST_AUTH, REDIS_CONNECTION, TEST_DB), log)
-- named result (is either string or {string[, ..]} for multi-bulk in multi-bulk, "[0]" is optional)
-- if a name is set, the result is not available via the result number.
result = r1.q({{"GET", "key_1"}, {"MGET", "key_2", "key_3"}}, {"key_1", {[0] = "multi_bulk", "key_2", "key_3"}})
print('query: {{"GET", "key_1"}, {"MGET", "key_2", "key_3"}}, {"key_1", {[0] = "multi_bulk", "key_2", "key_3"}}')
print(string.format("named result: %i{result[\"key_1\"][1] = \"%s\", %i{result[\"multi_bulk\"][\"key_2\"] = \"%s\", result.multi_bulk.key_3 = \"%s\"}}", #result, result["key_1"][1], #result["multi_bulk"], result["multi_bulk"]["key_2"], result.multi_bulk.key_3))
result = r.q({{"MGET", table.unpack(r.q({{"KEYS", "key_*"}})[1])}}, {{"key_1", [3] = "key_3"}})
print('query: {{"MGET", table.unpack(r.q({{"KEYS", "key_*"}})[1])}}, {{"key_1", [3] = "key_3"}}')
print(string.format("mixed result: %i{%i{result[1][\"key_1\"] = \"%s\", result[1][2] = \"%s\", result[1][3] = \"%s\"}}", #result, #result[1], result[1]["key_1"], result[1][2], result[1][3] or "nil"))
result = r.q({{"MGET", "key_1", "key_2"}, {"MGET", "key_1", "key_2"}}, {nil, true})
print('query: {{"MGET", "key_1", "key_2"}, {"MGET", "key_1", "key_2"}}, {nil, true}')
print(string.format("normal result, values true: %i{%i{result[1][1] = \"%s\", result[1][2] = \"%s\"}, %i{result[2][1] = \"%s\", result[2][\"key_1\"] = \"%s\", result[2][\"value_1\"] = \"%s\"}, result[2][\"value_2\"] = \"%s\"}}", #result, #result[1], result[1][1], result[1][2], #result[2], result[2][1] or "nil", result[2]["key_1"] or "nil", result[2]["value_1"], result[2]["value_2"]))
print(string.format("FLUSHDB -> %s", r1.q({{"FLUSHDB"}})[1][1]))
-- test reconnect
if RECONNECT_TIMEOUT > 0 then
  print(string.format("Test timeout reconnect .. sleep %d", RECONNECT_TIMEOUT))
  os.execute(string.format("sleep %d", RECONNECT_TIMEOUT))
end
r.close()
result = r1.q({{"KEYS", "*"}, {"EXISTS", "none"}})
print(string.format("KEYS *, EXISTS none -> %s, %d", (result[1][1] or "nil"), result[2][1]))
r1.close()
-- reconnect works even after connection was closed
print(string.format("DBSIZE -> %d", r1.q({{"DBSIZE"}})[1][1]))
r1.close()

